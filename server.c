#include <stdio.h>
#include <unistd.h>
#include <string.h>

#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>

#define DEFAULT_PORT 43

int main() {
    struct sockaddr_in sa, client_addr;
    int sockfd, client;

    int addr_len = sizeof(sa);

    char username[256] = {0};
    char password[256] = {0};

    char *hello = "220 \r\n";
    char *pass_request = "331 \r\n";
    char *login_success = "320 \r\n";

    if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        perror("Failed creating socket");
        return 1;
    }

    sa.sin_family = AF_INET;
    sa.sin_addr.s_addr = INADDR_ANY;
    sa.sin_port = htons(DEFAULT_PORT);

    if (bind(sockfd, (struct sockaddr *)&sa, sizeof(sa)) < 0) {
        perror("Failed to bind");
        return 1;
    }

    if (listen(sockfd, 10) < 0) {
        perror("Listen");
        return 1;
    }
    
    while (client = accept(sockfd, (struct sockaddr *)&client_addr, (socklen_t*)&addr_len)) {

        printf("Received connection\n");

        send(client, hello, strlen(hello), 0);

        read(client, username, 1024);
        printf("%s", username);

        send(client, pass_request, strlen(pass_request), 0);
        read(client, password, 1024);
        printf("%s", password);

        send(client, login_success, strlen(login_success), 0);
    }

    close(sockfd);

    return 0;
}
